# Импорт функции рандома целых чисел из модуля random

from random import randint

# Функция для решения 1 задания 

def Quest1():
    while True:
        try:
            number = int(input("Введите число: "))
        except ValueError:
            print("Вы должны ввести число, попробуйте снова.")
        if number > 0:
            num = list(range(1, number + 1))
            buf = 1
            sum = 0
            for i in num:
                buf = buf * i
                sum += buf
            print(sum)
            break
        else:
            print("Число должно быть больше 0")
        

# Функция для решения 2 задания

def Quest2():
    a = []
    for i in range(20):
        i = i
        a.append(randint(-10, 10))
    print(a)
    neg = []
    pos = []
    for i in a:
        if i < 0:
            neg.append(i)
        elif i > 0:
            pos.append(i)
    print(pos)
    print("Число положительных элементов массива: " + str(len(pos)))


def Quest3():
    a = []
    b = []
    for i in range(20):
        i = i
        a.append(randint(-10, 10))
    print(a)
    b = list(a)
    b.sort() #sort использует сортировку Timsort
    print(b)
    while True:
        try:
            number = int(input("Введите число которое нужно найти: "))
            break
        except ValueError:
            print("Вы должны ввести число, попробуйте снова.")
    low = 0
    high = 19
    while low <= high:
        mid = (low + high) // 2
        if number < b[mid]:
            high = mid - 1
        elif number > b[mid]:
            low = mid + 1
        else:
            print("\nID для отсортированного массива = " + str(mid) + "\n")
            for z in range(20):
                if a[z] == b[mid]:
                    print("ID для неотсортированного массива =", z)
            break
    else:
        print("No the number")

# Функция для решения 4 задания 

def Quest4():
    a = []
    a.append(input("Введите стоку №1: "))
    a.append(input("Введите стоку №2: "))
    a.append(input("Введите стоку №3: "))
    a.append(input("Введите стоку №4: "))
    a.sort() #sort использует сортировку Timsort
    print(a)

# Функция для решения 5 задания 
def Quest5():
    while True:
        try:
            number = int(input("Введите шестизначное число: "))
            break
        except ValueError:
            print("Вы должны ввести число, попробуйте снова.")
    number = 1000000 + number
    y2 = number % 1000
    y1 = number // 1000
    y2 = y2 + 1000
    leng = len(str(number))
    if (leng == 7):
        x1 = y1 // 100
        x1 = x1 % 10
        x2 = (y1 // 10) % 10
        x3 = y1 % 10
        x4 = y2 // 100
        x4 = x4 % 10
        x5 = (y2 // 10) % 10
        x6 = y2 % 10
        print(x1, x2, x3, x4, x5, x6)
        if(x1 + x2 + x3) == (x4 + x5 + x6):
            print("Поздравляю, это счастливый билетик")
        else:
            print("В следующий раз поввезет")
    else:
        print("Число не шестизначное")

# Основная часть с выбором задания
quest = input("Введите номер задания: ")
if quest == '1':
    Quest1()
elif quest == '2':
    Quest2()
elif quest == '3':
    Quest3()
elif quest == '4':
    Quest4()
elif quest == '5':
    Quest5()
else:
    print("Такого задания нет ")
